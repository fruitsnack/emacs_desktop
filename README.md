# Fruitsnack’s EMACS EXWM Desktop
A custom all-in-one .emacs setup that I’ve been slowly working on.  
It relies on a minimal number of third-party packages and external tools with nearly all features custom-made.

## Custom-made features
- Automatic first-time setup and installation of required packages
- Management of desktop autostarts and environment variables
- .emacs-based compositor configuration
- Volume control
- Screenshot taking
- Keyboard layout switching
- Rendered whitespace, tabs and long line column
- Many dired tweaks to behave like a classic file manager
- Opening files based on extension in external programs or in emacs
- Custom extensible dired thumbnailer with extension-based icons
- Dynamic wallpaper management (creating collections, automatically changing wallpapers, etc)
- Dark mode for docview mode
- Toggleable transparent background
- Custom extensible splash screen with utility and app shortcuts
- Custom window layouts
- Minimalistic mode that centers text and hides distractions
- Extensible and themable custom mode (status) line
- Many custom segments for mode line, like workspace, audio player, VCS integration and more
- Custom theme engine that manages mode line and dired icon themes, wallpapers, fonts and many UI elements to allow completely switching emacs look on the fly
- Many more small tweaks and improvements (and things I forgot to list)

## Installation
1. Clone this repo somewhere (for example in your ~)
2. Symlink your ‘~/.emacs’ to ‘.emacs’ provided in this repo
3. Run emacs once so it’d install EXWM and symlink your ‘~/.xinitrc’ to its own
4. Start session with ‘startx’

## Required binaries
This setup relies on a number of external binaries (emacs already depends on
quite a number of them for its basic functionality):

- ‘startx’ - from ‘xorg-xinit‘, to actually start the X session
- ‘setxkbmap’ - from ‘xorg-setxkbmap‘, for layout switching
- ‘pactl’ - from ‘pulseaudio‘, to manage audio volume
- ‘picom’ - from ‘picom‘ (compton), window compositor
- ‘scrot’ - from ‘scrot‘, to take screenshots
- ‘feh’ - from ‘feh’, image viewer, setting wallpapers and more
- ’mpv’ - from ‘mpv’, EMMS backend, audio and video player
- ’convert‘ - from ‘imagemagick‘, to generate thumbnails and create dark mode pages in docview
- ’unzip‘ - from ‘unzip‘, to generate thumbnails
- ‘xcf2png’ - from ‘xcf-tools‘, to generate gimp thumbnails
- ‘ffmpegthumbnailer’ - from ‘ffmpegthumbnailer‘, to generate video thumbnails
- ‘blender-thumbnailer.py’ - from ‘blender‘, to generate thumbnails
- ‘gs’ - from ‘ghostscript‘, to generate ebook and document thumbnails
- ‘mutool’ - from ‘mupdf-tools‘, to generate ebook and document thumbnails
- ‘git’ - from ‘git’ - integration with VCS, required by magit

Package names correspond to arch linux and derivatives, so they
might be different on non-arch based distros.

## Roadmap
- xrandr-based screen color temperature and brightness management and corresponding mode line segments

Features are added spontaneously rather than planned
beforehand, so this list might stay empty for a while.

## Manual
Coming some day

## Keybinding list
Coming some day

## Screenshots
Coming some day

# License
Unless stated otherwise, all files in this repository are licensed under GNU Affero General Public License v3.0 only (AGPL-3.0-only).
