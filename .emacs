;###############################################################################
;INIT
;###############################################################################

(package-initialize)

;disable saving of "custom" settings
(setq custom-file (make-temp-file "emacs-custom"))

;###############################################################################
;AUTOMATIC PACKAGE INSTALLATION
;###############################################################################

(setq required-packages
  '(exwm
    emms
    nov
    magit
    base16-theme
    challenger-deep-theme
    nord-theme
    monokai-theme
    lua-mode
    go-mode
    rust-mode
    markdown-mode
))

;-------------------------------------------------------------------------------

(setq package-archives
  '(("gnu" . "https://elpa.gnu.org/packages/")
    ("melpa" . "https://melpa.org/packages/")))

(defun auto-install-missing-packages ()
  (let (pkglist)
    (dolist (p required-packages nil)
      (when (not (package-installed-p p))
        (add-to-list 'pkglist p)))
    (when (not (= (length pkglist) 0)) (package-refresh-contents nil)
          (dolist (p pkglist nil) (package-install p)))))

(auto-install-missing-packages)

;###############################################################################
;XINITRC AUTOLINK
;###############################################################################

(setq xinitrc-path (concat (getenv "HOME") "/" ".xinitrc"))
(setq exwm-xinitrc-path
  (concat (getenv "HOME") "/" ".emacs.d/elpa/exwm-0.24/xinitrc"))

;-------------------------------------------------------------------------------

(defun auto-link-xinitrc ()
  (if (file-exists-p xinitrc-path)
    (when (not
      (string= (concat (getenv "HOME") "/" (file-symlink-p xinitrc-path))
               exwm-xinitrc-path))
      (progn (rename-file xinitrc-path (concat xinitrc-path ".bak") t)
             (make-symbolic-link exwm-xinitrc-path xinitrc-path)))
    (make-symbolic-link exwm-xinitrc-path xinitrc-path)))

(auto-link-xinitrc)

;###############################################################################
;EXWM CONFIGURATION
;###############################################################################

(require 'exwm)
(require 'exwm-config)
(require 'exwm-systemtray)
(setq exwm-systemtray-height 18)
(setq exwm-systemtray-icon-gap 4)
(exwm-systemtray-enable)
(exwm-config-default)
(add-hook 'exwm-floating-setup-hook 'exwm-layout-hide-mode-line)

;###############################################################################
;EXWM DESKTOP ENV VARIABLES
;###############################################################################

(setenv "QT_QPA_PLATFORMTHEME" "qt5ct")
(setenv "KRITA_NO_STYLE_OVERRIDE" "1")

;###############################################################################
;EXWM DESKTOP AUTOSTARTS
;###############################################################################

(setq desktop-autostarts '(
;  ("udiskie" . "udiskie")
;  ("dunst" . "dunst")
;  ("syncthing" . "syncthing")
))

;-------------------------------------------------------------------------------

(mapc (lambda (elem)
  (start-process-shell-command (car elem) nil (cdr elem)))
    desktop-autostarts)

;###############################################################################
;COMPOSITOR
;###############################################################################

(setq picom-shadow-radius 18)
(setq picom-shadow-opacity 1)

(setq picom-opacity-rules '(
;  ("Discord" . 70)
;  ("Element" . 70)
;  ("Steam" . 70)
))

;-------------------------------------------------------------------------------

(defun picom-update-settings ()
  (setq picom-shadow-offset-x (- 0 picom-shadow-radius))
  (setq picom-shadow-offset-y (- 0 picom-shadow-radius))
  (setq picom-arguments `(
    ("shadow-radius" . ,picom-shadow-radius)
    ("shadow-opacity" . ,picom-shadow-opacity)
    ("shadow-offset-x" . ,picom-shadow-offset-x)
    ("shadow-offset-y" . ,picom-shadow-offset-y)
  )))

(defun start-picom ()
  (when (process-live-p (get-process "Picom"))
    (delete-process (get-process "Picom")))
  (start-process-shell-command "Picom" nil
    (concat "picom " (mapconcat
      (lambda (elem) (if (cdr elem)
        (concat "--" (car elem) "=" (number-to-string (cdr elem)))
        (concat "--" (car elem))))
      picom-arguments " ") " "
        (mapconcat (lambda (elem)
          (concat "--opacity-rule '"
          (number-to-string (cdr elem)) ":name *= \"" (car elem) "\"'"))
          picom-opacity-rules " "))))

;###############################################################################
;GLOBAL KEY BINDINGS
;###############################################################################

;EXWM Keybindings
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([?\s-F] . enlarge-window-horizontally))
  '(([?\s-B] . shrink-window-horizontally))
  '(([?\s-N] . shrink-window))
  '(([?\s-P] . enlarge-window))
  '(([?\s-p] . windmove-up))
  '(([?\s-n] . windmove-down))
  '(([?\s-b] . windmove-left))
  '(([?\s-f] . windmove-right))
  '(([?\s-M] . exwm-workspace-move-window))
  '(([?\C-x ?\p] . (lambda nil (interactive) (other-window -1))))
))

;Layout Switching
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([8388640] . keyboard-layout-next))
  '(([?\s-c] . exwm-input-toggle-keyboard))
))

;Audio Volume
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([?\s-\-] . (lambda nil (interactive) (volume-change nil))))
  '(([?\s-=] . (lambda nil (interactive) (volume-change t))))
  '(([?\s-m] . volume-mute-toggle))
))

;Screenshots
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([print] . (lambda nil (interactive) (take-screenshot))))
  '(([s-print] . (lambda nil (interactive) (take-screenshot t))))
))

;EMMS Keybindings
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([8388709 98] . emms-smart-browse))
  '(([8388709 115] . emms-stop))
  '(([8388709 99] . emms-playlist-current-clear))
  '(([?\s-e ?\p] . emms-pause))
  '(([?\s-\]] . emms-seek-forward))
  '(([?\s-\[] . emms-seek-backward))
  '(([?\s-\\] . emms-next))
  '(([?\s-\'] . emms-previous))
  ))

;Utilities Quickstart
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([8388723 101] . (lambda nil (interactive) (eshell 'new))))
  '(([8388723 100] . (lambda nil (interactive) (dired "~/"))))
  '(([8388723 111] . (lambda nil (interactive) (find-file (car
    org-agenda-files)))))
  '(([8388723 116] . (lambda nil (interactive) (ansi-term "/bin/bash"))))
  '(([8388723 112] . proced))
  '(([8388723 99] . calendar))
  '(([8388723 120] . calculator))
  '(([8388723 119] . wallpaper-set-random-wallpaper))
  '(([8388723 98] . splash-create-buffer))
  '(([8388723 103] . gnus))
  '(([8388723 115] . start-interactive-application))
  '(([8388724 97] . bg-alpha-toggle))
  '(([8388724 108] . toggle-mode-line))
  '(([8388724 103 108] . toggle-mode-line-global))
  '(([8388724 109] . minimalistic-mode-toggle))
  '(([8388724 99] . (lambda nil (interactive) (minimalistic-mode-toggle t))))
  '(([8388652] . minimalistic-mode-increase-width))
  '(([8388654] . minimalistic-mode-decrease-width))
  '(([8388724 100] . (lambda nil (interactive) (window-divider-mode 'toggle))))
))

;Buffer switching
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([?\C-x ?\C-b] . ibuffer))
  '(([?\C-x ?\K] . kill-some-buffers))
  '(([?\C-c ?\K] . kill-all-buffers))
  ))

;Window layouts
(setq exwm-input-global-keys (append exwm-input-global-keys
  '(([8388641] . layout-split-modern))
  '(([8388672] . layout-split-modern-x))
  '(([8388643] . layout-split-ide))
  '(([8388644] . layout-split-movie))
  '(([8388645] . layout-split-two-thirds))
  '(([8388702] . layout-split-two-thirds-split))
  '(([8388646] . layout-split-centered))
  '(([8388650] . layout-split-centered-split))
))

(global-set-key (kbd "C-x 2")
  (lambda nil (interactive) (split-window-splash t)))

(global-set-key (kbd "C-x 3")
  (lambda nil (interactive) (split-window-splash nil)))

;###############################################################################
;DESKTOP UTILITIES
;###############################################################################

(defun kill-all-buffers ()
  (interactive)
  (when (y-or-n-p "Kill all buffers?")
    (kill-matching-buffers ".*" nil t)))

(defun start-interactive-application (command)
  (interactive (list (read-shell-command "Start program: ")))
  (start-process-shell-command command nil command))

;###############################################################################
;TAKING SCREENSHOTS
;###############################################################################

(setq screenshot-normal-args (mapconcat 'identity
      '(
        "-q 50"
        "-p"
        "~/Pictures/Screenshot_%d-%m-%Y-%H-%M-%S.png"
) " "))

(setq screenshot-select-args (mapconcat 'identity
      '("-s"
        "-f"
        "-l style=dash,width=1,opacity=60,mode=edge"
) " "))

;-------------------------------------------------------------------------------

(defun take-screenshot (&optional rect)
  (let ((cmd (if rect
      (concat "scrot" " " screenshot-select-args " " screenshot-normal-args)
      (concat "scrot" " " screenshot-normal-args)
     )))
    (start-process-shell-command "" nil cmd)
))

;###############################################################################
;EMACS GENERAL CONFIGURATION
;###############################################################################

(ido-mode t)

(setq ido-enable-flex-matching t)
(setq user-full-name "vanta")
(setq user-mail-address "none")
(setq ring-bell-function 'ignore)

(setq initial-scratch-message nil)
(setq inhibit-startup-screen -1)
(put 'list-timers 'disabled nil)

(defalias 'yes-or-no-p 'y-or-n-p)

;###############################################################################
;EMACS EDITING CONFIGURATION
;###############################################################################

(global-visual-line-mode t)
(transient-mark-mode t)

(setq kill-ring-max 1000)
(setq save-inteprogram-paste-before-kill t)
(setq x-select-enable-clipboard t)

(setq-default indent-tabs-mode nil)
(setq-default standard-indent 4)
(setq tab-always-ident 'complete)

(setq-default fill-column 80)

(electric-pair-mode t)
(electric-quote-mode t)

(dynamic-completion-mode t)

(delete-selection-mode t)
(setq-default tab-width 4)

(setq create-lockfiles t)
(setq large-file-warning-threshold 100000000)
(setq out-of-memory-warning-percentage nil)

(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq backup-by-copying t)
(setq make-backup-files t)

(setq auto-save-default t)
(setq auto-save-interval 300)

(setq auto-revert-interval 10)
(setq global-auto-revert-non-file-buffers nil)
(global-auto-revert-mode t)

(show-paren-mode t)
(setq show-parens-when-point-inside-paren t)

(setq global-hl-line-sticky-flag t)
(global-hl-line-mode t)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;###############################################################################
;WHITESPACE MODE CONFIGURATION
;###############################################################################

(require 'whitespace)

(setq whitespace-style '(face trailing tabs spaces lines-tail
  space-before-tab space-after-tab space-mark tab-mark))

(defun whitespace-mode-fix-faces ()
  (set-face-attribute 'whitespace-space nil :inherit
    'font-lock-comment-delimiter-face)
  (set-face-attribute 'whitespace-space nil :background 'unspecified)
  (set-face-attribute 'whitespace-space nil :foreground 'unspecified)
  (set-face-attribute 'whitespace-tab nil :inherit
    'font-lock-comment-delimiter-face)
  (set-face-attribute 'whitespace-tab nil :background 'unspecified)
  (set-face-attribute 'whitespace-tab nil :foreground 'unspecified)
  (set-face-attribute 'whitespace-line nil :background
    (face-attribute 'font-lock-comment-delimiter-face :foreground)))

(add-hook 'prog-mode-hook 'whitespace-mode)
(add-hook 'whitespace-mode-hook 'whitespace-mode-fix-faces)

;###############################################################################
;DISPLAY FILL COLUMN INDICATOR MODE CONFIGURATION
;###############################################################################

(setq-default display-fill-column-indicator-character 32)

(defun display-fill-column-indicator-mode-fix-faces ()
  (set-face-attribute
   'fill-column-indicator nil :inherit 'font-lock-comment-delimiter-face)
  (set-face-attribute 'fill-column-indicator nil :background
    (face-attribute 'font-lock-comment-delimiter-face :foreground)))

(add-hook 'prog-mode-hook 'display-fill-column-indicator-mode)
(add-hook 'display-fill-column-indicator-mode-hook
  'display-fill-column-indicator-mode-fix-faces)

;###############################################################################
;PROCED CONFIGURATION
;###############################################################################

(setq-default proced-auto-update-flag nil)
(setq proced-auto-update-interval 1)
(setq-default proced-tree-flag t)
(setq-default proced-filter 'user)

(defun proced-visual-line-mode-fix ()
  (visual-line-mode -1)
  (setq truncate-lines t))

(add-hook 'proced-mode-hook 'proced-visual-line-mode-fix)

;###############################################################################
;IBUFFER CONFIGURATION
;###############################################################################

(add-hook 'ibuffer-mode-hook
  (lambda nil (local-set-key (kbd "<mouse-1>") 'ibuffer-visit-buffer)))

;###############################################################################
;GNUS
;###############################################################################

(setq gnus-use-full-window nil)

;###############################################################################
;NOV CONFIGURATION
;###############################################################################

(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
(setq nov-text-width t)

(defun nov-font-setup ()
  (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
                           :height 1.3))

(add-hook 'nov-mode-hook 'nov-font-setup)
(add-hook 'nov-mode-hook 'minimalistic-mode-enable)

;###############################################################################
;MARKDOWN CONFIGURATION
;###############################################################################

(add-hook 'markdown-mode-hook
  (lambda nil (interactive) (minimalistic-mode-toggle t)))

;###############################################################################
;DIRED CONFIGURATION
;###############################################################################

(require 'dired)

(setq dired-file-associations
  #s(hash-table
    test equal
      data (
        "png" "feh -. --start-at"
        "jpg" "feh -. --start-at"
        "jpeg" "feh -. --start-at"
        "bmp" "feh -. --start-at"
        "webp" "feh -. --start-at"
        "gif" "mpv --loop-file=inf"
        "webm" "mpv --loop-file=inf"
        "mp4" "mpv -loop-file=inf"
        "mov" "mpv -loop-file=inf"
        "mkv" "mpv -loop-file=inf"
        "ts" "mpv -loop-file=inf"
        "kra" "krita"
        "ora" "krita"
        "psd" "krita"
        "blend" "blender"
        "blend1" "blender"
        "xcf" "gimp"
        "exe" "wine"
        "mp3" "mpv -loop-file=inf"
        "ogg" "mpv -loop-file=inf"
        "flac" "mpv -loop-file=inf"
        "wav" "mpv -loop-file=inf"
        "aiff" "mpv -loop-file=inf"
        "m4a" "mpv -loop-file=inf"
        "pdf" "mupdf"
        "epub" "mupdf"
        "fb2" "mupdf"
        "jar" "java -jar"
)))

(setq dired-mode-line-spacing 3)

;-------------------------------------------------------------------------------

(setq dired-recursive-deletes 'always)
(setq dired-recursive-copies 'always)
(setq dired-dwim-target t)

(put 'dired-find-alternate-file 'disabled nil)

(setq dired-permanent-hide-details-mode t)

(setq dired-sorting-modes '(
  "time"
  "size"
  "extension"
))

(setq dired-sorting-mode-index 0)

(setq dired-listing-switches-hash-table
  #s(hash-table
    test equal
      data (
        reverse nil
        groups "--no-group"
        human-readable "--human-readable"
        sorting "--sort=time"
        group-dirs "--group-directories-first"
        hidden nil
        long "-l"
)))

(defun dired-update-current-buffer ()
  (interactive)
  (when (string= major-mode "dired-mode")
  (find-alternate-file default-directory)))

(defun dired-update-listing-switches ()
  (setq dired-listing-switches
    (let (
      (result nil)
      (arg-list
        (let (temp-list) 
          (progn
            (maphash (lambda (key elem) (push elem temp-list))
              dired-listing-switches-hash-table)
            temp-list))))
      (while arg-list
        (let ((elem (pop arg-list)))
          (when elem (setq result (concat result elem
            (when (or (car arg-list) (cdr arg-list)) " "))))))
      result))
  (dired-update-current-buffer))

(defun dired-toggle-hidden ()
  (interactive)
  (if (gethash 'hidden dired-listing-switches-hash-table)
      (progn (puthash 'hidden nil dired-listing-switches-hash-table)
             (message "Showing hidden files is disabled"))
    (progn (puthash 'hidden "-A" dired-listing-switches-hash-table)
    (message "Showing hidden files is enabled")))
  (dired-update-listing-switches))

(defun dired-toggle-group-dirs ()
  (interactive)
  (if (gethash 'group-dirs dired-listing-switches-hash-table)
      (progn (puthash 'group-dirs nil dired-listing-switches-hash-table)
             (message "Displaying directories first is disabled"))
    (progn (puthash 'group-dirs
      "--group-directories-first" dired-listing-switches-hash-table)
    (message "Displaying directories first is enabled")))
  (dired-update-listing-switches))

(defun dired-toggle-reverse ()
  (interactive)
  (if (gethash 'reverse dired-listing-switches-hash-table)
      (progn (puthash 'reverse nil dired-listing-switches-hash-table)
             (message "Reverse listing is disabled"))
    (progn (puthash 'reverse
      "--reverse" dired-listing-switches-hash-table)
    (message "Reverse listing is enabled")))
  (dired-update-listing-switches))

(defun dired-cycle-sorting-modes ()
  (interactive)
  (if (< dired-sorting-mode-index (- (length dired-sorting-modes) 1))
      (setq dired-sorting-mode-index (+ dired-sorting-mode-index 1))
    (setq dired-sorting-mode-index 0))
  (puthash 'sorting
    (concat "--sort=" (nth dired-sorting-mode-index dired-sorting-modes))
    dired-listing-switches-hash-table)
  (message (concat
   "Dired sorting mode: "
   (nth dired-sorting-mode-index dired-sorting-modes)))
  (dired-update-listing-switches))

(defun dired-toggle-hide-details-mode ()
  (interactive)
  (setq dired-permanent-hide-details-mode
    (not dired-permanent-hide-details-mode))
  (if dired-permanent-hide-details-mode
      (dired-hide-details-mode)
    (dired-hide-details-mode 0)))

(defun dired-check-for-hide-details-on-reload ()
  (interactive)
  (when dired-permanent-hide-details-mode
    (dired-hide-details-mode)))

(defun dired-external-open (file)
  (let ((ext (car (last (split-string file "\\.")))))
    (if (gethash ext dired-file-associations)
      (start-process-shell-command ""
        nil (gethash ext dired-file-associations)
          (concat "\"" file "\""))
    (find-file file))))

(defun dired-find-dir-or-ext-open ()
  (interactive)
  (let ((find-file-run-dired t)
    (file (dired-get-file-for-visit)))
    (if (file-directory-p file)
      (dired-find-alternate-file)
      (dired-external-open file)
)))

(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "<mouse-2>") 'dired-find-dir-or-ext-open)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "RET") #'dired-find-dir-or-ext-open)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "a") #'dired-find-dir-or-ext-open)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "u") (lambda nil (interactive)
    (find-alternate-file "..")))))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "&") (lambda nil (interactive)
    (start-process-shell-command ""
      nil (read-from-minibuffer "Open with: ")
      (concat "\"" (dired-get-file-for-visit) "\""))))))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-c h") 'dired-toggle-hidden)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-c g") 'dired-toggle-group-dirs)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-c r") 'dired-toggle-reverse)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-c m") 'dired-cycle-sorting-modes)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "g") 'dired-update-current-buffer)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-x w") 'dired-add-files-to-wallpaper-list)))
(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-c d") 'dired-toggle-hide-details-mode)))

(add-hook 'dired-mode-hook
  (lambda nil (setq line-spacing dired-mode-line-spacing)))

(dired-update-listing-switches)

(add-hook 'dired-after-readin-hook 'dired-check-for-hide-details-on-reload)

;###############################################################################
;DIRED THUMBNAILS
;###############################################################################

(setq micro-thumbs-thumb-format "jpg")
(setq micro-thumbs-thumb-height 127)
(setq micro-thumbs-icon-height 1.3)
(setq micro-thumbs-pad-thumbnails t)
(setq micro-thumbs-pad-color "#808080")
(setq micro-thumbs-frame-thumbnails t)
(setq micro-thumbs-frame-color "#909090")
(setq micro-thumbs-tmp-file-path "/tmp/micro_thumb.png")

(setq micro-thumbs-base-icon '("" picoline-separator-end-8))
(setq micro-thumbs-folder-icon '("" picoline-separator-end-4))
(setq micro-thumbs-symlink-icon '("" picoline-separator-end-8))
(setq micro-thumbs-text-icon '("" picoline-separator-end-10))
(setq micro-thumbs-image-icon '("" picoline-separator-end-3))
(setq micro-thumbs-video-icon '("" picoline-separator-end-6))
(setq micro-thumbs-audio-icon '("" picoline-separator-end-1))
(setq micro-thumbs-pdf-icon '("" picoline-separator-end-5))
(setq micro-thumbs-spreadsheet-icon '("" picoline-separator-end-0))
(setq micro-thumbs-document-icon '("" picoline-separator-end-0))
(setq micro-thumbs-presentation-icon '("" picoline-separator-end-0))
(setq micro-thumbs-archive-icon '("" picoline-separator-end-7))
(setq micro-thumbs-code-icon '("" picoline-separator-end-9))
(setq micro-thumbs-script-icon '("" picoline-separator-end-2))
(setq micro-thumbs-binary-icon '("" picoline-separator-end-0))
(setq micro-thumbs-win-binary-icon '("" picoline-separator-end-0))
(setq micro-thumbs-book-icon '("" picoline-separator-end-5))
(setq micro-thumbs-blend-icon '("" picoline-separator-end-4))
(setq micro-thumbs-ps-icon '("" picoline-separator-end-1))
(setq micro-thumbs-markup-icon '("" picoline-separator-end-0))
(setq micro-thumbs-secure-icon '("" picoline-separator-end-7))
(setq micro-thumbs-cfg-icon '("" picoline-separator-end-9))
(setq micro-thumbs-git-icon '("" picoline-separator-end-0))
(setq micro-thumbs-jar-icon '("" picoline-separator-end-5))

(setq micro-thumbs-size-string
  (if micro-thumbs-pad-thumbnails
    (concat "'%h%x%h%>' -background \"" micro-thumbs-pad-color "\"")
    "%h%x%h%^"))

(setq micro-thumbs-frame-string
  (when micro-thumbs-frame-thumbnails
   (concat "-border 1x1 -bordercolor \"" micro-thumbs-frame-color "\"")))

(setq micro-thumbs-convert-arguments
  (mapconcat 'identity
    `("-thumbnail"
       ,micro-thumbs-size-string
       "-gravity center"
       "-extent %h%x%h%"
       ,micro-thumbs-frame-string
       "-strip \"jpeg:%o%\"") " "))

(setq micro-thumbs-convert-base-command
  (concat "convert \"%i%[0]\" " micro-thumbs-convert-arguments))
(setq micro-thumbs-convert-pipe-command
  (concat "convert - " micro-thumbs-convert-arguments))
(setq micro-thumbs-convert-tmp-command
  (concat "convert \""
    micro-thumbs-tmp-file-path "\" " micro-thumbs-convert-arguments))

(setq micro-thumbs-image-thumbnailer-command
  micro-thumbs-convert-base-command)
(setq micro-thumbs-video-thumbnailer-command
  (concat "ffmpegthumbnailer -i \"%i%\" -c png -s 0 -o - | "
    micro-thumbs-convert-pipe-command))
(setq micro-thumbs-kra-thumbnailer-command
  (concat "unzip -p \"%i%\" preview.png | " micro-thumbs-convert-pipe-command))
(setq micro-thumbs-blend-thumbnailer-command
  (concat "blender-thumbnailer.py \"%i%\" \"" micro-thumbs-tmp-file-path
    "\" && " micro-thumbs-convert-tmp-command))
(setq micro-thumbs-xcf-thumbnailer-command
  (concat "xcf2png \"%i%\" -o - | " micro-thumbs-convert-pipe-command))
(setq micro-thumbs-pdf-thumbnailer-command
  (concat "gs -dSAFER -dNOPAUSE -dTextAlphaBits=4 -dBATCH \
-dGraphicsAlphaBits=4 -dQUIET -sDEVICE=png16m -r50 -dFirstPage=1 -dLastPage=1 \
-sOutputFile=- \"%i%\" | " micro-thumbs-convert-pipe-command))
(setq micro-thumbs-epub-thumbnailer-command
  (concat "mutool draw -f -F png -o - \"%i%\" 1 | "
    micro-thumbs-convert-pipe-command))

(setq micro-thumbs-ext-thumbnailers
  #s(hash-table
    test equal
      data (
        "png" micro-thumbs-image-thumbnailer-command
        "jpg" micro-thumbs-image-thumbnailer-command
        "jpeg" micro-thumbs-image-thumbnailer-command
        "bmp" micro-thumbs-image-thumbnailer-command
        "webp" micro-thumbs-image-thumbnailer-command
        "gif" micro-thumbs-image-thumbnailer-command
        "mp4" micro-thumbs-video-thumbnailer-command
        "avi" micro-thumbs-video-thumbnailer-command
        "mov" micro-thumbs-video-thumbnailer-command
        "webm" micro-thumbs-video-thumbnailer-command
        "wmv" micro-thumbs-video-thumbnailer-command
        "ts" micro-thumbs-video-thumbnailer-command
        "kra" micro-thumbs-kra-thumbnailer-command
        "blend" micro-thumbs-blend-thumbnailer-command
        "blend1" micro-thumbs-blend-thumbnailer-command
        "xcf" micro-thumbs-xcf-thumbnailer-command
        "pdf" micro-thumbs-epub-thumbnailer-command
        "epub" micro-thumbs-epub-thumbnailer-command
        "fb2" micro-thumbs-epub-thumbnailer-command
        )))

(setq micro-thumbs-ext-icons
  #s(hash-table
    test equal
      data (
            "png" micro-thumbs-image-icon
            "jpeg" micro-thumbs-image-icon
            "jpg" micro-thumbs-image-icon
            "bmp" micro-thumbs-image-icon
            "gif" micro-thumbs-image-icon
            "webp" micro-thumbs-image-icon
            "tiff" micro-thumbs-image-icon
            "svg" micro-thumbs-image-icon
            "zip" micro-thumbs-archive-icon
            "7z" micro-thumbs-archive-icon
            "tar" micro-thumbs-archive-icon
            "gz" micro-thumbs-archive-icon
            "rar" micro-thumbs-archive-icon
            "bz2" micro-thumbs-archive-icon
            "xz" micro-thumbs-archive-icon
            "lz" micro-thumbs-archive-icon
            "mp4"  micro-thumbs-video-icon
            "ts" micro-thumbs-video-icon
            "webm" micro-thumbs-video-icon
            "avi"  micro-thumbs-video-icon
            "wmv" micro-thumbs-video-icon
            "3gp" micro-thumbs-video-icon
            "mov" micro-thumbs-video-icon
            "mkv" micro-thumbs-video-icon
            "mp3" micro-thumbs-audio-icon
            "ogg" micro-thumbs-audio-icon
            "wav" micro-thumbs-audio-icon
            "flac" micro-thumbs-audio-icon
            "xcf" micro-thumbs-image-icon
            "ora" micro-thumbs-image-icon
            "kra" micro-thumbs-image-icon
            "blend" micro-thumbs-blend-icon
            "blend1" micro-thumbs-blend-icon
            "psd" micro-thumbs-ps-icon
            "pdf" micro-thumbs-pdf-icon
            "txt" micro-thumbs-text-icon
            "log" micro-thumbs-text-icon
            "org" micro-thumbs-text-icon
            "doc" micro-thumbs-document-icon
            "docx" micro-thumbs-document-icon
            "odt" micro-thumbs-document-icon
            "ppt" micro-thumbs-presentation-icon
            "pptx" micro-thumbs-presentation-icon
            "odp" micro-thumbs-presentation-icon
            "xls" micro-thumbs-spreadsheet-icon
            "xlsx" micro-thumbs-spreadsheet-icon
            "ods" micro-thumbs-spreadsheet-icon
            "html" micro-thumbs-markup-icon
            "xml" micro-thumbs-markup-icon
            "json" micro-thumbs-markup-icon
            "yaml" micro-thumbs-markup-icon
            "toml" micro-thumbs-markup-icon
            "opf" micro-thumbs-markup-icon
            "md" micro-thumbs-markup-icon
            "sh" micro-thumbs-script-icon
            "py" micro-thumbs-script-icon
            "el" micro-thumbs-script-icon
            "lua" micro-thumbs-script-icon
            "bat" micro-thumbs-script-icon
            "vbs" micro-thumbs-script-icon
            "c" micro-thumbs-code-icon
            "cpp" micro-thumbs-code-icon
            "h" micro-thumbs-code-icon
            "hpp" micro-thumbs-code-icon
            "rs" micro-thumbs-code-icon
            "go" micro-thumbs-code-icon
            "java" micro-thumbs-code-icon
            "php" micro-thumbs-code-icon
            "asm" micro-thumbs-code-icon
            "ino" micro-thumbs-code-icon
            "js" micro-thumbs-code-icon
            "sln" micro-thumbs-code-icon
            "gradle" micro-thumbs-code-icon
            "smali" micro-thumbs-code-icon
            "csproj" micro-thumbs-code-icon
            "cmake" micro-thumbs-code-icon
            "so" micro-thumbs-binary-icon
            "class" micro-thumbs-binary-icon
            "pyc" micro-thumbs-binary-icon
            "a" micro-thumbs-binary-icon
            "o" micro-thumbs-binary-icon
            "dll" micro-thumbs-win-binary-icon
            "exe" micro-thumbs-win-binary-icon
            "msi" micro-thumbs-win-binary-icon
            "lib" micro-thumbs-win-binary-icon
            "epub" micro-thumbs-book-icon
            "mobi" micro-thumbs-book-icon
            "fb2" micro-thumbs-book-icon
            "chm" micro-thumbs-book-icon
            "djvu" micro-thumbs-book-icon
            "kbdx" micro-thumbs-secure-icon
            "kbd" micro-thumbs-secure-icon
            "eds" micro-thumbs-secure-icon
            "asc" micro-thumbs-secure-icon
            "pgp" micro-thumbs-secure-icon
            "cert" micro-thumbs-secure-icon
            "sec" micro-thumbs-secure-icon
            "cfg" micro-thumbs-cfg-icon
            "conf" micro-thumbs-cfg-icon
            "config" micro-thumbs-cfg-icon
            "ini" micro-thumbs-cfg-icon
            "emacs" micro-thumbs-cfg-icon
            "vimrc" micro-thumbs-cfg-icon
            "xprofile" micro-thumbs-cfg-icon
            "bashrc" micro-thumbs-cfg-icon
            "bash_history" micro-thumbs-cfg-icon
            "xinitrc" micro-thumbs-cfg-icon
            "lesshst" micro-thumbs-cfg-icon
            "jackrc" micro-thumbs-cfg-icon
            "gitignore" micro-thumbs-git-icon
            "gitconfig" micro-thumbs-git-icon
            "jar" micro-thumbs-jar-icon
            )))

;-------------------------------------------------------------------------------

(setq micro-thumbs-show-icons t)
(setq micro-thumbs-show-thumbnails t)
(setq micro-thumbs-thumb-dir (concat (getenv "HOME") "/.emacs.d/micro-thumbs/"))
(setq micro-thumbs-queue '())
(setq micro-thumbs-timer-handle nil)
(setq micro-thumbs-queue-interval 0.05)

(defun micro-thumbs-file-ext (path)
(let ((ind (string-match "\\.[^\\.]*$" path)))
  (if ind (downcase (substring path (+ 1 ind))) "")))

(defun micro-thumbs-put-icon (icon pos)
  (overlay-put (make-overlay pos pos) 'before-string
    (propertize (concat " " (car icon) " ") 'face (cdr icon) 'display
      `(height ,micro-thumbs-icon-height))))

(defun micro-thumbs-get-unique-thumbnail (filepath)
  (format "%s_%s.%s"
          (md5 (file-name-directory filepath))
          (file-name-base filepath)
          micro-thumbs-thumb-format))

(defun micro-thumbs-get-thumbnailer-command (filepath height)
  (let ((cmd (eval
   (gethash
    (micro-thumbs-file-ext filepath)
    micro-thumbs-ext-thumbnailers)))
      result)
    (setq result (replace-regexp-in-string "\\%i\\%" filepath cmd))
    (setq result (replace-regexp-in-string "\\%o\\%" (concat
      micro-thumbs-thumb-dir
      (micro-thumbs-get-unique-thumbnail filepath))
        result))
    (setq result (replace-regexp-in-string "\\%h\\%"
      (int-to-string height) result))))

(defun micro-thumbs-process-queue ()
  (if micro-thumbs-queue
      (start-process-shell-command "" nil (pop micro-thumbs-queue))
    (progn (cancel-timer micro-thumbs-timer-handle)
           (setq micro-thumbs-timer-handle nil))))

(defun micro-thumbs-add-thumbnail-to-queue (filepath)
  (when (not micro-thumbs-timer-handle)
    (setq micro-thumbs-timer-handle
      (run-at-time nil micro-thumbs-queue-interval
        'micro-thumbs-process-queue)))
  (add-to-list 'micro-thumbs-queue
    (micro-thumbs-get-thumbnailer-command filepath
      micro-thumbs-thumb-height)
    t))

(defun micro-thumbs-get-or-create-thumb (filepath)
  (let ((thumb-path (micro-thumbs-get-unique-thumbnail filepath)))
      (when (not (file-exists-p (concat micro-thumbs-thumb-dir thumb-path)))
        (when (not (file-directory-p micro-thumbs-thumb-dir))
          (make-directory micro-thumbs-thumb-dir t))
        (micro-thumbs-add-thumbnail-to-queue filepath))
      (concat micro-thumbs-thumb-dir thumb-path)))

(defun micro-thumbs-clear-thumbnail-cache ()
  (interactive)
  (when (yes-or-no-p "Delete ALL generated thumbnails?")
    (start-process-shell-command "" nil
      (concat "rm " micro-thumbs-thumb-dir "*"))))

(defun micro-thumbs-show ()
  (interactive)
  (when (or micro-thumbs-show-icons micro-thumbs-show-thumbnails)
  (if (string= major-mode "dired-mode")
      (save-excursion
        (remove-overlays)
    (let (file dir (ind (point-min)))
      (goto-char (point-min))
      (while (not (eobp))
        (save-excursion
          (and
           (not (eolp))
           (setq file (dired-get-filename nil t))
           (progn
             (setq ind (point))
             (setq dir (looking-at-p dired-re-dir))
             (setq sym (looking-at-p dired-re-sym))
             (end-of-line)
             (if (and dir micro-thumbs-show-icons)
               (micro-thumbs-put-icon micro-thumbs-folder-icon ind)
               (if (and sym micro-thumbs-show-icons)
                 (micro-thumbs-put-icon micro-thumbs-symlink-icon ind)
               (progn (when (and micro-thumbs-show-icons
                 (not (and micro-thumbs-show-thumbnails
                   (gethash (micro-thumbs-file-ext file)
                     micro-thumbs-ext-thumbnailers))))
                 (let ((icon (eval (gethash
                   (micro-thumbs-file-ext file) micro-thumbs-ext-icons))))
                   (if icon (micro-thumbs-put-icon icon ind)
                     (micro-thumbs-put-icon micro-thumbs-base-icon ind))))
                 (when (and micro-thumbs-show-thumbnails
                   (gethash (micro-thumbs-file-ext file)
                     micro-thumbs-ext-thumbnailers))
                   (put-image (create-image
                     (micro-thumbs-get-or-create-thumb file))
                     ind)))))
             )))
        (forward-line 1)
        )))
  (message "Not in dired mode!"))))

(defun micro-thumbs-switch-modes ()
  (interactive)
  (if (and (not micro-thumbs-show-thumbnails) (not micro-thumbs-show-icons))
      (progn (setq micro-thumbs-show-icons t)
      (message "Showing icons"))
    (if (and (not micro-thumbs-show-thumbnails) micro-thumbs-show-icons)
      (progn (setq micro-thumbs-show-thumbnails t)
      (setq micro-thumbs-show-icons nil)
      (message "Showing thumbnails"))
    (if (and micro-thumbs-show-thumbnails (not micro-thumbs-show-icons))
      (progn (setq micro-thumbs-show-icons t)
      (message "Showing icons and thumbnails"))
    (if (and micro-thumbs-show-thumbnails micro-thumbs-show-icons)
      (progn (setq micro-thumbs-show-icons nil)
      (setq micro-thumbs-show-thumbnails nil)
      (message "Showing none")))))))

(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "C-t") 'micro-thumbs-switch-modes)))

(add-hook 'dired-after-readin-hook 'micro-thumbs-show)

(advice-add 'dired-do-copy :after 'micro-thumbs-show)
(advice-add 'dired-do-rename :after 'micro-thumbs-show)
(advice-add 'dired-do-delete :after 'micro-thumbs-show)

;###############################################################################
;RCIRC CONFIGURATION
;###############################################################################

(setq rcirc-server-alist '(
                          ("irc.libera.chat" :nick "gugingus" :port 6697 :encryption tls)
                          ))

;###############################################################################
;DYNAMIC WALLPAPERS
;###############################################################################

(setq wallpaper-change-time 3600)
(setq wallpaper-change-wallpapers t)

;-------------------------------------------------------------------------------

(setq wallpaper-collection '())
(setq wallpaper-list-current '())
(setq wallpaper-file-path "~/.emacs.d/wallpaper-collection")

(defun wallpaper-write-list-to-file ()
  (interactive)
  (write-region (format "%S" wallpaper-collection) nil wallpaper-file-path))

(defun wallpaper-read-list-from-file ()
  (interactive)
  (if (file-exists-p wallpaper-file-path)
  (setq wallpaper-collection (car (read-from-string
    (with-temp-buffer (insert-file-contents
                       wallpaper-file-path)
                      (buffer-string)))))
  (setq wallpaper-collection '())))

(defun dired-add-files-to-wallpaper-list ()
  (interactive)
  (if (string= major-mode "dired-mode")
      (let ((current-list (dired-get-marked-files))
        (collection-name (read-from-minibuffer "Collection name: "
          nil nil nil nil (car (car wallpaper-collection)))))
        (if (assoc collection-name wallpaper-collection)
          (let
            ((newlist (delete-dups (append current-list (cdr
              (assoc collection-name wallpaper-collection))))))
            (setq wallpaper-collection (assoc-delete-all collection-name
              wallpaper-collection))
            (add-to-list 'wallpaper-collection
              (cons collection-name (delete-dups newlist))))
          (add-to-list 'wallpaper-collection
            (cons collection-name (delete-dups current-list))))
        (wallpaper-write-list-to-file)
        (message "Added marked files to the wallpaper list!"))
      (message "Not in Dired mode!")))

(defun wallpaper-clear-collection ()
  (interactive)
  (let ((collection-name (read-from-minibuffer
    "Collection name: " nil nil nil nil (car (car wallpaper-collection)))))
    (if (assoc collection-name wallpaper-collection)
        (progn (setq wallpaper-collection
                     (assoc-delete-all collection-name wallpaper-collection))
                 (message "Cleared wallpaper collection"))
      (message "No such collection!"))
      (wallpaper-write-list-to-file)))

(defun wallpaper-set-current-collection (&optional collection)
  (interactive)
  (if collection (setq wallpaper-list-current
    (cdr (assoc collection wallpaper-collection)))
    (setq wallpaper-list-current (cdr (assoc (read-from-minibuffer
      "Collection name: " nil nil nil nil
      (car (car wallpaper-collection)))
   wallpaper-collection)))))

(defun wallpaper-set-random-wallpaper ()
  (interactive)
  (when (and wallpaper-change-wallpapers
    (not (= (length wallpaper-list-current) 0)))
    (start-process-shell-command "" nil
      (concat "feh --no-fehbg --bg-fill \""
              (nth (random (length wallpaper-list-current))
                   wallpaper-list-current) "\""))))

(wallpaper-read-list-from-file)

(setq wallpaper-change-timer
  (run-at-time t wallpaper-change-time 'wallpaper-set-random-wallpaper))

;###############################################################################
;IMAGE MODE CONFIGURATION
;###############################################################################

(add-hook 'image-mode-hook (lambda nil (image-toggle-animation)))
(add-hook 'image-mode-hook (lambda nil (image-transform-fit-to-height)))

;###############################################################################
;DOC-VIEW MODE CONFIGURATION
;###############################################################################

(defun doc-view-invert-colors ()
  (interactive)
  (if (string= major-mode "doc-view-mode")
      (progn
        (message "doc-view conversion in progress...")
      (dotimes (page-number (+ 1 (doc-view-last-page-number)))
      (start-process-shell-command "imagemagick" nil
        (concat "convert -negate " "\""
                (concat (doc-view--current-cache-dir) "page-"
                        (number-to-string page-number) ".png") "\""
                " \"" (concat (doc-view--current-cache-dir) "page-"
                        (number-to-string page-number) ".png")  "\"")))
           (clear-image-cache)
           (doc-view-goto-page (doc-view-current-page))
           (message "Finished doc-view conversion"))
    (message "Not in doc-view-mode!")))

(add-hook 'dired-mode-hook
  (lambda nil (local-set-key (kbd "S-r") 'doc-view-invert-colors)))

;###############################################################################
;ORG-MODE CONFIGURATION
;###############################################################################

(setq org-agenda-files '("~/Documents/ORG/Notebook.org"))
(setq org-enforce-todo-dependencies t)

(add-hook 'org-mode-hook
  (lambda nil (interactive) (minimalistic-mode-toggle t)))
(add-hook 'org-mode-hook 'org-overview)
(add-hook 'org-mode-hook 'org-indent-mode)

;###############################################################################
;ESHELL CONFIGURATION
;###############################################################################

(setq eshell-visual-commands '(
  "htop"
  "top"
  "iotop"
  "nano"
  "vim"
  "vi"
  "toxic"
  "nmtui"
  "nyx"
  "ncdu"
))

(setq eshell-visual-subcommands '(
  "git"
))

;###############################################################################
;ARTIST MODE CONFIGURATION
;###############################################################################

(add-hook 'artist-mode-hook
  (lambda nil (interactive) (setq-default artist-default-fill-char 35)))
(add-hook 'artist-mode-hook
  (lambda nil (local-set-key (kbd "C-c C-a d") 'artist-select-op-pen-line)))
(add-hook 'artist-mode-hook
  (lambda nil (local-set-key (kbd "C-c C-a e") 'artist-select-op-erase-rectangle)))

;###############################################################################
;SUPERCOLLIDER MODE CONFIGURATION
;###############################################################################

(require 'sclang)

;###############################################################################
;EMMS CONFIGURATION
;###############################################################################

(require 'emms-setup)
(emms-all)
(emms-default-players)
(setq emms-source-file-default-directory "~/Music")
(setq emms-browser-covers 'emms-browser-cache-thumbnail-async)
(emms-playing-time-disable-display)

;###############################################################################
;VISUAL CONFIGURATION
;###############################################################################

(setq use-dialog-box nil)
(setq exwm-floating-border-color "black")
(setq exwm-floating-border-width 1)

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(blink-cursor-mode -1)
(tooltip-mode -1)

;###############################################################################
;BACKGROUND ALPHA CONFIGURATION
;###############################################################################

(setq bg-alpha 80)
(setq bg-alpha-default-enabled t)

;-------------------------------------------------------------------------------

(defun bg-alpha-toggle ()
  (interactive)
  (if (eq (car (frame-parameter nil 'alpha)) 100)
      (progn
        (set-frame-parameter nil 'alpha `(,bg-alpha . ,bg-alpha))
        (message "Enabled frame transparency"))
      (progn
        (set-frame-parameter nil 'alpha `(100 . 100))
        (message "Disabled frame transparency"))))

(defun bg-alpha-setup-frame (frame)
  (if bg-alpha-default-enabled
    (set-frame-parameter frame 'alpha `(,bg-alpha . ,bg-alpha))
    (set-frame-parameter frame 'alpha `(100 . 100))))

(add-hook 'after-make-frame-functions 'bg-alpha-setup-frame)

;###############################################################################
;SPLASH SCREEN
;###############################################################################

(setq splash-buffer-name "Bootstrap")
(setq splash-ascii-graphics "
          ________
         /\\       \\
        /  \\       \\
       /    \\       \\
      /      \\_______\\
      \\      /       /
    ___\\    /   ____/___
   /\\   \\  /   /\\       \\
  /  \\   \\/___/  \\       \\
 /    \\       \\   \\       \\
/      \\_______\\   \\_______\\
\\      /       /   /       /
 \\    /       /   /       /
  \\  /       /\\  /       /
   \\/_______/  \\/_______/
")

(setq splash-favorites '(
  ("blender" . "Blender")
  ("gimp" . "Gimp")
  ("krita" . "Krita")
  ("obs" . "OBS Studio")
  ("discord" . "Cuckold Simulator")
  ("element-desktop" . "Element")
  ("steam" . "Steam")
  ("firefox" . "Firefox")
))

(setq splash-quick-actions '(
  ("Start program" . (lambda nil (call-interactively
    'start-interactive-application)))
  ("Open a file" . (lambda nil (call-interactively 'find-file)))
  ("Open ORG Notebook" . (lambda nil (find-file (car org-agenda-files))))
  ("Open ~/.emacs" . (lambda nil (find-file "~/.emacs")))
  ("Open home directory" . (lambda nil (dired "~/")))
  ("Start supercollider" . (lambda nil (sclang-start)))
  ("Open EMMS" . (lambda nil (emms-smart-browse)))
  ("EShell" . (lambda nil (eshell)))
  ("Ansi-Term" . (lambda nil (ansi-term "/bin/bash")))
  ("Proced" . (lambda nil (proced)))
  ("RCIRC" . (lambda nil (rcirc)))
  ("GNUS" . (lambda nil (gnus)))
))

;-------------------------------------------------------------------------------

(defface splash-button '((t
  :foreground "black"
  :background "#757575"
  :weight bold
  :box (:line-width 3 :style released-button)
  :underline nil
  ))
nil)

(defface splash-button-pressed '((t
  :foreground "black"
  :background "#909090"
  :weight bold
  :box (:line-width 3 :style released-button)
  :underline nil
  ))
nil)

(defun splash-insert-ascii ()
  (insert splash-ascii-graphics))

(defun splash-insert-quick-actions ()
  (fancy-splash-insert :face 'picoline-segment-4 " Quick Actions "
    :face 'picoline-separator-end-4 mode-line-left-separator)
  (dolist (i splash-quick-actions)
    (insert "\n")
    (insert-button
     (concat " " (car i) " ")
     'face 'splash-button
     'mouse-face 'splash-button-pressed
     'action `(lambda (button)
       (let ((buf (current-buffer))) (funcall ,(cdr i)) (kill-buffer buf)))
     'follow-link t
     )))

(defun splash-insert-favorites ()
  (fancy-splash-insert :face 'picoline-segment-7 " Quick Launch "
    :face 'picoline-separator-end-7 mode-line-left-separator)
  (dolist (i splash-favorites)
    (insert "\n")
    (insert-button
     (concat " " (cdr i) " ")
     'face 'splash-button
     'mouse-face 'splash-button-pressed
     'action `(lambda (button)
       (let ((buf (current-buffer)))
         (start-process-shell-command ,(car i) nil ,(car i)) (kill-buffer buf)))
     'follow-link t
     )))

(defun splash-insert-vert-spacer ()
  (save-excursion
    (goto-char (point-min))
    (insert (make-string
      (max (/ (- (window-total-height)
          (count-lines (point-min) (point-max)))
         2)
       0)
     ?\n))))

(defun splash-longest-buffer-line ()
  (interactive)
  (save-excursion
  (let ((len 0))
    (goto-char (point-min))
    (while (not (eobp))
      (when (> (- (line-end-position) (line-beginning-position)) len)
        (setq len  (- (line-end-position) (line-beginning-position))))
      (forward-line 1)
      )
    len
)))

(defun splash-create-buffer (&optional window)
  (interactive)
  (let ((wind (or window (selected-window)))
                (buf (generate-new-buffer splash-buffer-name)))
    (set-window-buffer wind buf)
    (save-selected-window (select-window wind)
      (save-excursion
        (splash-insert-ascii)
        (insert "\n\n")
        (splash-insert-quick-actions)
        (insert "\n\n")
        (splash-insert-favorites)
        (splash-insert-vert-spacer)
        (string-rectangle
          (point-min)
          (save-excursion (goto-char (point-max)) (beginning-of-line) (point))
          (make-string (/ (max (- (window-total-width)
                                  (splash-longest-buffer-line))
                               0)
                          2)
                       ?\ ))
        (setq buffer-read-only t)
        (set-buffer-modified-p nil)))
    buf))

(defun split-window-splash (&optional vert)
  (setq wind (if vert (split-window-vertically) (split-window-horizontally)))
  (splash-create-buffer wind))

;###############################################################################
;WINDOW LAYOUTS
;###############################################################################

(defun window-width-percent (percent)
  (floor (* (/ (float (window-total-width)) 100) percent)))

(defun window-height-percent (percent)
  (floor (* (/ (float (window-total-height)) 100) percent)))

(defun window-resize-by-percent (percent dir &optional window)
  (let (total-width total-height)
    (progn (save-window-excursion (delete-other-windows)
      (setq total-width (window-total-width))
      (setq total-height (window-total-height)))
      (window-resize window
        (floor (* (/
          (float (if dir total-width total-height)) 100)
          percent))
         dir))))

(defun layout-split-modern ()
  (interactive)
  (delete-other-windows)
  (save-selected-window
    (select-window (split-window nil (window-width-percent 70) 'right))
    (split-window nil (window-height-percent 60) 'below))
  (save-selected-window
    (select-window (split-window nil (window-height-percent 70) 'below))
    (split-window nil (window-width-percent 50) 'right)))

(defun layout-split-modern-x ()
  (interactive)
  (delete-other-windows)
  (save-selected-window
    (select-window (split-window nil (window-width-percent 70) 'right))
    (split-window nil (window-height-percent 66) 'below)
    (split-window nil (window-height-percent 50) 'below))
  (save-selected-window
    (select-window (split-window nil (window-height-percent 70) 'below))
    (split-window nil (window-width-percent 50) 'right))
  (split-window nil (window-width-percent 65) 'right))

(defun layout-split-ide ()
  (interactive)
  (delete-other-windows)
  (save-selected-window
    (select-window (split-window nil (window-height-percent 70) 'below))
    (split-window nil (window-width-percent 75) 'right)
    (split-window nil (window-width-percent 66) 'right)
    (split-window nil (window-width-percent 50) 'right))
  (save-selected-window
    (split-window nil (window-width-percent 80) 'right)
    (split-window nil (window-width-percent 24) 'right)))

(defun layout-split-movie ()
  (interactive)
  (delete-other-windows)
  (save-selected-window
    (select-window (split-window nil (window-height-percent 75) 'below))
    (split-window nil (window-width-percent 75) 'right)
    (split-window nil (window-width-percent 66) 'right)
    (split-window nil (window-width-percent 50) 'right)))

(defun layout-split-centered ()
  (interactive)
  (delete-other-windows)
  (split-window nil (window-width-percent 70) 'right)
  (split-window nil (window-width-percent 44) 'right))

(defun layout-split-centered-split ()
  (interactive)
  (delete-other-windows)
  (split-window nil (window-width-percent 70) 'right)
  (save-selected-window
    (select-window (split-window nil (window-width-percent 44) 'right))
    (split-window nil (window-height-percent 50) 'below)))

(defun layout-split-two-thirds ()
  (interactive)
  (delete-other-windows)
  (split-window nil (window-width-percent 60) 'right))

(defun layout-split-two-thirds-split ()
  (interactive)
  (delete-other-windows)
  (save-selected-window
    (select-window (split-window nil (window-width-percent 60) 'right))
    (split-window nil (window-height-percent 50) 'below)))

;###############################################################################
;MINIMALISTIC MODE
;###############################################################################

(setq minimalistic-mode-desired-width 115)
(setq minimalistic-mode-text-increase 1)
(setq minimalistic-mode-width-step 5)

;-------------------------------------------------------------------------------

(setq minimalistic-mode-enabled nil)
(make-variable-buffer-local 'minimalistic-mode-enabled)

(defun minimalistic-mode-resize-margins ()
  (let ((margin
    (/ (max (- (window-total-width) minimalistic-mode-desired-width) 0) 2)))
    (set-window-margins nil margin margin)))

(defun minimalistic-mode-enable (&optional preserve-mode-line)
  (interactive)
  (add-hook 'window-configuration-change-hook
    'minimalistic-mode-resize-margins t t)
  (minimalistic-mode-resize-margins)
  (text-scale-increase minimalistic-mode-text-increase)
  (when (not preserve-mode-line) (mode-line-disable))
  (setq minimalistic-mode-enabled t)
  (message "Minimalistic mode enabled"))

(defun minimalistic-mode-disable (&optional preserve-mode-line)
  (interactive)
  (remove-hook 'window-configuration-change-hook
    'minimalistic-mode-resize-margins t)
  (set-window-margins nil 0 0)
  (text-scale-mode 0)
  (when (not preserve-mode-line) (mode-line-enable))
  (kill-local-variable 'minimalistic-mode-enabled)
  (message "Minimalistic mode disabled"))

(defun minimalistic-mode-toggle (&optional preserve-mode-line)
  (interactive)
  (if minimalistic-mode-enabled
    (minimalistic-mode-disable preserve-mode-line)
    (minimalistic-mode-enable preserve-mode-line)))

(defun minimalistic-mode-increase-width ()
  (interactive)
  (setq minimalistic-mode-desired-width (+ minimalistic-mode-desired-width
    minimalistic-mode-width-step))
  (minimalistic-mode-resize-margins)
  (message (concat "Minimalistic mode width is: "
    (number-to-string minimalistic-mode-desired-width) " columns")))

(defun minimalistic-mode-decrease-width ()
  (interactive)
  (setq minimalistic-mode-desired-width (- minimalistic-mode-desired-width
   minimalistic-mode-width-step))
  (when (< minimalistic-mode-desired-width 0)
    (setq minimalistic-mode-desired-width 0))
  (minimalistic-mode-resize-margins)
  (message (concat "Minimalistic mode width is: "
    (number-to-string minimalistic-mode-desired-width) " columns")))

;###############################################################################
;MODE LINE VOLUME CONTROL AND INDICATOR
;###############################################################################

(setq volume-sink-number 1)
(setq mode-line-volume-mute-icon "婢")
(setq mode-line-volume-0-icon "")
(setq mode-line-volume-25-icon "")
(setq mode-line-volume-50-icon "墳")
(setq mode-line-volume-75-icon "")

;-------------------------------------------------------------------------------

(setq mode-line-volume-icon nil)
(setq volume-level 50)
(setq volume-level-string nil)
(setq volume-mute nil)

(defun volume-update-icon ()
  (if volume-mute (setq mode-line-volume-icon mode-line-volume-mute-icon)
    (if (>= volume-level 75)
        (setq mode-line-volume-icon mode-line-volume-75-icon)
      (if (>= volume-level 50)
          (setq mode-line-volume-icon mode-line-volume-50-icon)
        (if (>= volume-level 25)
            (setq mode-line-volume-icon mode-line-volume-25-icon)
          (setq mode-line-volume-icon mode-line-volume-0-icon))))))

(defun volume-set-volume ()
  (start-process-shell-command
   "pactl" nil
    (concat "pactl set-sink-volume "
    (number-to-string volume-sink-number) " "
    (number-to-string volume-level) "%")))

(defun volume-set-mute (enabled)
  (start-process-shell-command
    "pactl" nil
    (concat "pactl set-sink-mute" " "
    (number-to-string volume-sink-number) " "
    (if enabled "1" "0"))))

(defun volume-update-string ()
  (if volume-mute
      (setq volume-level-string "MUTED")
  (setq volume-level-string
        (concat (number-to-string volume-level) "%%")))
  (volume-update-icon)
  (force-mode-line-update))

(defun volume-change (increase)
  (if increase
      (progn
        (if (< volume-level 100)
            (setq volume-level (+ volume-level 5)))
    (message (concat "Increased volume to: " (number-to-string volume-level))))
    (progn (if (> volume-level 0)
               (setq volume-level (- volume-level 5)))
    (message (concat "Decreased volume to: " (number-to-string volume-level)))))
  (volume-update-string)
  (volume-set-volume))

(defun volume-mute-toggle ()
  (interactive)
  (setq volume-mute (not volume-mute))
  (volume-set-mute volume-mute)
  (if volume-mute (message "Muted audio") (message "Unmuted audio"))
  (volume-update-string))

(volume-set-volume)
(volume-update-string)

;###############################################################################
;MODE LINE EMMS INDICATOR
;###############################################################################

(setq mode-line-emms-title-icon "")
(setq emms-stopped-icon "")
(setq emms-paused-icon "")
(setq emms-playing-icon "")
(setq emms-track-name-max-length 35)
(setq emms-mode-line-redisplay-interval 1)

;-------------------------------------------------------------------------------

(setq emms-elapsed-total-time-placeholder "-:-/-:-")
(setq emms-elapsed-total-time-string nil)
(setq mode-line-emms-time-icon nil)
(setq emms-redisplay-timer nil)

(defun emms-check-start-redisplay-timer ()
  (when (not emms-redisplay-timer)
    (setq emms-redisplay-timer
           (run-at-time t emms-mode-line-redisplay-interval
                        'emms-set-current-time-string))))

(defun emms-check-stop-redisplay-timer ()
  (when emms-redisplay-timer
  (cancel-timer emms-redisplay-timer)
  (setq emms-redisplay-timer nil)))

(defun emms-start-or-stop-progress-timer ()
  (if emms-player-playing-p
      (if emms-player-paused-p
          (emms-check-stop-redisplay-timer)
        (emms-check-start-redisplay-timer)
        )
    (emms-check-stop-redisplay-timer)))

(defun emms-update-status-icon ()
  (if emms-player-playing-p
      (if emms-player-paused-p
          (setq mode-line-emms-time-icon emms-paused-icon)
        (setq mode-line-emms-time-icon emms-playing-icon))
    (setq mode-line-emms-time-icon emms-stopped-icon)))

(defun emms-set-current-time-string ()
  (if emms-player-playing-p
(setq emms-elapsed-total-time-string
  (concat
   (format-seconds "%.2h:%z%m:%.2s" emms-playing-time)
   "/"
   (format-seconds "%.2h:%z%m:%.2s" (emms-track-get
   (emms-playlist-current-selected-track) 'info-playing-time))))
(setq emms-elapsed-total-time-string emms-elapsed-total-time-placeholder)))

(defun emms-status-update-string (&optional sec)
  (let ((str (emms-track-description (emms-playlist-current-selected-track))))
    (if (string= str "nil: ")
        (setq emms-status-string "No Track")
      (setq emms-status-string
            (if (> (length str) emms-track-name-max-length)
              (concat (substring str 0 emms-track-name-max-length) "...")
              str
            ))))
  (emms-update-status-icon)
  (emms-start-or-stop-progress-timer)
  (emms-set-current-time-string)
  (force-mode-line-update))

(defun emms-print-progress-indicator (&optional sec)
  (let* ((total-playing-time (emms-track-get
                              (emms-playlist-current-selected-track)
                              'info-playing-time))
         (elapsed/total (/ (* 100 emms-playing-time) total-playing-time)))
    (with-temp-message (format "[%-100s] %2d%%"
                               (make-string elapsed/total ?=)
                               elapsed/total)
      (sit-for 2))))

(add-hook 'emms-player-seeked-functions 'emms-print-progress-indicator)
(add-hook 'emms-player-seeked-functions 'emms-status-update-string)

(add-hook 'emms-player-started-hook 'emms-status-update-string)
(add-hook 'emms-player-stopped-hook 'emms-status-update-string)
(add-hook 'emms-player-cleared-hook 'emms-status-update-string)
(add-hook 'emms-player-finished-hook 'emms-status-update-string)
(add-hook 'emms-player-paused-hook 'emms-status-update-string)

(emms-status-update-string)

;###############################################################################
;MODE LINE KEYBOARD LAYOUT INDICATOR
;###############################################################################

(setq mode-line-layout-icon "")
(setq keyboard-layouts '("us" "ru"))

;-------------------------------------------------------------------------------

(setq keyboard-layout (car keyboard-layouts))
(setq keyboard-layout-index 0)

(defun keyboard-layout-set ()
  (interactive)
  (setq keyboard-layout (nth keyboard-layout-index keyboard-layouts))
  (start-process-shell-command "setxkbmap" nil
    (concat "setxkbmap " keyboard-layout))
  (force-mode-line-update t))

(defun keyboard-layout-next ()
  (interactive)
  (setq keyboard-layout-index (+ 1 keyboard-layout-index))
  (when (>= keyboard-layout-index (length keyboard-layouts))
    (setq keyboard-layout-index 0))
  (keyboard-layout-set)
  (message (concat "Switched to layout: " keyboard-layout)))

(keyboard-layout-set)

;###############################################################################
;MODE LINE EXWM WORKSPACE INDICATOR
;###############################################################################

(setq mode-line-workspace-icon "")
(setq workspace-decorate-icon-function 'workspace-decorate-circles)

;-------------------------------------------------------------------------------

(defun workspace-decorate-numbers (i selected)
  (if selected
      (concat "[" (number-to-string i) "]")
    (number-to-string i)))

(defun workspace-decorate-circles (i selected)
  (if selected "" ""))

(defun workspace-linemode-update-string ()
  (setq workspace-linemode-string (mapconcat
    'identity
    (let ((list ()))
      (dotimes (i (exwm-workspace--count) (nreverse list))
        (if (eq i (exwm-workspace--position exwm-workspace--current))
            (push (funcall workspace-decorate-icon-function i t) list)
            (push (funcall workspace-decorate-icon-function i nil) list)
          )))
    " ")))

(add-hook 'exwm-workspace-switch-hook 'workspace-linemode-update-string)

;###############################################################################
;MODE LINE CUSTOM CLOCK INDICATOR
;###############################################################################

(setq mode-line-calendar-icon "")
(setq mode-line-clock-icon "")

;-------------------------------------------------------------------------------

(defun update-custom-mode-line-clock ()
  (setq mode-line-custom-clock (format-time-string "%H:%M"))
  (setq mode-line-custom-calendar (format-time-string "%d/%m/%y"))
  (force-mode-line-update t))

(update-custom-mode-line-clock)

(setq time-update-timer (run-at-time t 60 'update-custom-mode-line-clock))

;###############################################################################
;MODE LINE GIT INDICATOR
;###############################################################################

(setq mode-line-git-icon "")

;-------------------------------------------------------------------------------

(setq mode-line-git-status-string "None")
(make-variable-buffer-local 'mode-line-git-status-string)

(defun mode-line-update-git-string ()
  (let
    ((gitstatus (split-string
      (shell-command-to-string "git status --porcelain=v2 --branch") "\n")))
    (if (string= "fatal" (car (split-string (car gitstatus) ":")))
        (setq mode-line-git-status-string "None")
    (let
      ((head
        (if (string= "branch.head" (nth 1 (split-string (nth 1 gitstatus) " ")))
          (nth 2 (split-string (nth 1 gitstatus) " "))
          "No head"))
        (remote
         (if (string= "branch.upstream"
           (nth 1 (split-string (nth 2 gitstatus) " ")))
             (nth 2 (split-string (nth 2 gitstatus) " "))
             "No Remote")))
      (setq mode-line-git-status-string (concat head " | " remote))))))

(add-hook 'find-file-hook 'mode-line-update-git-string)
(add-hook 'dired-mode-hook 'mode-line-update-git-string)
(add-hook 'eshell-mode-hook 'mode-line-update-git-string)
(add-hook 'eshell-directory-change-hook 'mode-line-update-git-string)

;###############################################################################
;MODE LINE THEMES
;###############################################################################

(setq picoline-theme-cascade '(
  ("#0d122a" . "#2dc7e9")
  ("#0d122a" . "#3d9dff")
  ("#0d122a" . "#4d53ff")
  ("#0d122a" . "#7a41f5")
  ("#0d122a" . "#b648ee")
  ("#0d122a" . "#e45cdf")
  ("#0d122a" . "#df7da2")
  ("#0d122a" . "#24c3a9")
  ("#0d122a" . "#ff7f68")
  ("#0d122a" . "#ffa95e")
  ("#0d122a" . "#ffa595")
  ("#0d122a" . "#ffd685")
))

(setq picoline-theme-sunset '(
  ("#fff5d7" . "#160c59")
  ("#fff5d7" . "#2f106a")
  ("#fff5d7" . "#471166")
  ("#fff5d7" . "#750d8a")
  ("#fff5d7" . "#9d075a")
  ("#fff5d7" . "#ba0c30")
  ("#fff5d7" . "#cf0009")
  ("#120d30" . "#ff5a10")
  ("#120d30" . "#ff9342")
  ("#120d30" . "#ffbe5a")
  ("#120d30" . "#ffdaa2")
  ("#120d30" . "#fff7c4")
))

(setq picoline-theme-nord '(
  ("#2e3440" . "#5e81ac")
  ("#2e3440" . "#81a1c1")
  ("#2e3440" . "#88c0d0")
  ("#2e3440" . "#8fbcbb")
  ("#2e3440" . "#ebcb8b")
  ("#2e3440" . "#d08770")
  ("#2e3440" . "#bf616a")
  ("#2e3440" . "#a3be8c")
  ("#2e3440" . "#b48ead")
  ("#2e3440" . "#d8dee9")
  ("#2e3440" . "#e5e9f0")
  ("#2e3440" . "#eceff4")
  ("#d8dee9" . "#4c566a")
))

(setq picoline-theme-black-metal '(
  ("#f0f0f0" . "#303030")
  ("#f0f0f0" . "#353535")
  ("#f0f0f0" . "#404040")
  ("#f0f0f0" . "#454545")
  ("#f0f0f0" . "#505050")
  ("#f0f0f0" . "#555555")
  ("#f0f0f0" . "#606060")
  ("#f0f0f0" . "#656565")
  ("#f0f0f0" . "#707070")
  ("#f0f0f0" . "#757575")
  ("#f0f0f0" . "#808080")
  ("#f0f0f0" . "#858585")
  ("#f0f0f0" . "#909090")
))

(setq picoline-theme-dracula '(
  ("#f8f8f2" . "#6272a4")
  ("#44475a" . "#8be9fd")
  ("#44475a" . "#50fa7b")
  ("#44475a" . "#ffb86c")
  ("#44475a" . "#ff79c6")
  ("#44475a" . "#bd93f9")
  ("#f8f8f2" . "#ff5555")
  ("#f8f8f2" . "#282a36")
  ("#f8f8f2" . "#44475a")
  ("#44475a" . "#f1fa8c")
  ("#f8f8f2" . "#44475a")
  ("#282a36" . "#f8f8f2")
))

(setq picoline-theme-monokai-pro '(
  ("#2D2A2E" . "#FF6188")
  ("#2D2A2E" . "#FC9867")
  ("#2D2A2E" . "#FFD866")
  ("#2D2A2E" . "#A9DC76")
  ("#2D2A2E" . "#78DCE8")
  ("#2D2A2E" . "#AB9DF2")
  ("#272822" . "#F92672")
  ("#272822" . "#FD971F")
  ("#272822" . "#E6DB74")
  ("#272822" . "#A6E22E")
  ("#272822" . "#66D9EF")
  ("#272822" . "#AE81FF")
  ("#FFFFFF" . "#171812")
))

(setq mode-line-height-multiplier 1.6)
(setq mode-line-height-adjust 0.18)

;-------------------------------------------------------------------------------

(defun picoline-define-segment-set (inp-theme pref)
  (dotimes (i (length inp-theme))
    (progn
    (face-spec-set
      (car (read-from-string (concat pref "-segment-" (number-to-string i))))
        `((t :foreground ,(car (nth i inp-theme))
             :background ,(cdr (nth i inp-theme))
             :weight bold)))
    (face-spec-set
     (car (read-from-string (concat pref
       "-separator-end-" (number-to-string i))))
      `((t :foreground ,(cdr (nth i inp-theme))
           :height ,mode-line-height-multiplier
           :weight bold)))
    ))
  (dotimes (i (length inp-theme))
    (when (not (>= i (- (length inp-theme) 1)))
    (face-spec-set
      (car (read-from-string (concat pref "-separator-" (number-to-string i))))
        `((t :foreground ,(cdr (nth i inp-theme))
             :background ,(cdr (nth (+ i 1) inp-theme))
             :height ,mode-line-height-multiplier
             :weight bold))))))

(defun picoline-set-theme (inp-theme)
;  (picoline-define-segment-set (car inp-theme) "left")
  (picoline-define-segment-set inp-theme "picoline"))

;###############################################################################
;WINDOW DIVIDERS
;###############################################################################

(setq window-divider-width-chars 1)
(setq window-divider-edge-face 'picoline-segment-8)
(setq window-divider-draw-edges t)

;-------------------------------------------------------------------------------

(setq window-divider-default-places 'right-only)
(setq window-divider-default-right-width
  (* (default-line-height) window-divider-width-chars))

(defun window-divider-set-faces ()
  (interactive)
  (set-face-attribute
    'window-divider nil :foreground
    (face-attribute 'mode-line-inactive :background))
  (let
    ((color
      (if window-divider-draw-edges
        (face-attribute window-divider-edge-face :background)
        (face-attribute 'mode-line-inactive :background))))
  (set-face-attribute
    'window-divider-first-pixel nil :foreground color)
  (set-face-attribute
    'window-divider-last-pixel nil :foreground color)))

(add-hook 'window-divider-mode-hook 'window-divider-set-faces)

;###############################################################################
;MODE LINE CONFIGURATION
;###############################################################################

(setq mode-line-truncate-right t)
(setq mode-line-draw-separators t)
(setq mode-line-show-icons t)
(setq mode-line-draw-outline t)
(setq mode-line-separator-padding 1)
(setq mode-line-left-separator "")
(setq mode-line-right-separator "")
(setq mode-line-name-icon "")

;-------------------------------------------------------------------------------

(setq mode-line-format-form
  '((:eval (split-mode-line mode-line-left-segments mode-line-right-segments))))
(setq-default mode-line-format mode-line-format-form)
(setq mode-line-shown t)

(defun split-mode-line (left right)
  (let ((available-width
    (- (window-total-width)
      (+ (length (format-mode-line left)) (length (format-mode-line right)))
      (/ (window-right-divider-width) (default-font-width))
      (- (ceiling (* (float (* mode-line-segment-amount
        (+ 1 mode-line-separator-padding)))
        mode-line-height-multiplier))
          (* mode-line-segment-amount (+ 1 mode-line-separator-padding))))))
    (if (and mode-line-truncate-right (< available-width 0))
      left
      (append left (list (format
        (format "%%%ds" (if (< available-width 0) 0 available-width)) ""))
        right))))

(defun mode-line-seg (func seg-face &optional seg-symbol)
  (let ((segment (if (and seg-symbol mode-line-show-icons)
    `(concat " " ,seg-symbol "  " ,func " ")
    `(concat " " ,func " "))))
    `(:eval (propertize ,segment 'face ',seg-face 'display
      '(raise ,mode-line-height-adjust)))))

(defun mode-line-sep (right sep-face)
      (setq mode-line-segment-amount (+ 1 mode-line-segment-amount))
  (when mode-line-draw-separators
  (let ((separator-symbol
         (concat (if right mode-line-right-separator mode-line-left-separator)
                 (format (format "%%%ds" mode-line-separator-padding) ""))))
    `(:eval (propertize ,separator-symbol 'face ',sep-face))
    )))

(setq mode-line-left-segment-list '(
  (mode-line-sep nil 'picoline-separator-end-8)
  (mode-line-seg 'mode-line-git-status-string 'picoline-segment-8
    'mode-line-git-icon)
  (mode-line-sep nil 'picoline-separator-7)
  (mode-line-seg "%2b" 'picoline-segment-7 'mode-line-name-icon)
))

(setq mode-line-right-segment-list '(
  (mode-line-seg 'mode-line-custom-clock 'picoline-segment-0
    'mode-line-clock-icon)
  (mode-line-sep t 'picoline-separator-0)
  (mode-line-seg 'mode-line-custom-calendar 'picoline-segment-1
    'mode-line-calendar-icon)
  (mode-line-sep t 'picoline-separator-1)
  (mode-line-seg 'volume-level-string 'picoline-segment-2
    'mode-line-volume-icon)
  (mode-line-sep t 'picoline-separator-2)
  (mode-line-seg 'keyboard-layout 'picoline-segment-3
    'mode-line-layout-icon)
  (mode-line-sep t 'picoline-separator-3)
  (mode-line-seg 'workspace-linemode-string 'picoline-segment-4
    'mode-line-workspace-icon)
  (mode-line-sep t 'picoline-separator-4)
  (mode-line-seg 'emms-elapsed-total-time-string
    'picoline-segment-5 'mode-line-emms-time-icon)
  (mode-line-sep t 'picoline-separator-5)
  (mode-line-seg 'emms-status-string 'picoline-segment-6
    'mode-line-emms-title-icon)
  (mode-line-sep t 'picoline-separator-end-6)
))

(defun mode-line-regenerate ()
  (interactive)
  (setq mode-line-left-segments '())
  (setq mode-line-right-segments '())
  (setq mode-line-segment-amount 0)
  (mapc (lambda (elem) (interactive)
          (let ((segm (eval elem)))
            (when segm
          (add-to-list 'mode-line-left-segments segm))
          ))
        mode-line-left-segment-list)
  (mapc (lambda (elem) (interactive)
          (let ((segm (eval elem)))
            (when segm
          (add-to-list 'mode-line-right-segments segm))
          ))
        mode-line-right-segment-list)
  (let ((box-attributes
    (if mode-line-draw-outline
        `(:line-width 1 :color
          ,(face-attribute window-divider-edge-face :background))
      nil)))
  (set-face-attribute 'mode-line nil :box box-attributes)
(set-face-attribute 'mode-line-inactive nil :box box-attributes)))

(defun toggle-mode-line-global ()
  (interactive)
  (if mode-line-shown
    (progn
      (setq mode-line-shown nil)
      (setq-default mode-line-format nil)
      (message "Mode line is hidden globally"))
    (progn
      (setq mode-line-shown t)
      (setq-default mode-line-format mode-line-format-form)
      (message "Mode line is visible globally")))
  (redraw-display))

(defun mode-line-disable ()
  (interactive)
  (setq mode-line-format nil)
  (setq header-line-format nil)
  (redraw-frame))

(defun mode-line-enable ()
  (interactive)
  (kill-local-variable 'mode-line-format)
  (kill-local-variable 'header-line-format)
  (redraw-frame))

(defun toggle-mode-line ()
  (interactive)
  (if mode-line-format
    (progn
      (mode-line-disable)
      (message "Mode line is hidden"))
    (progn
      (mode-line-enable)
      (message "Mode line is visible"))))

;###############################################################################
;EMACS LOOK CONFIGURATION
;###############################################################################

(setq emacs-look-nord '(
  :theme nord
  :mode-line-theme picoline-theme-nord
  :mode-line-separators t
  :mode-line-icons t
  :mode-line-outline t
  :mode-line-height 1.6
  :mode-line-adjust 0.18
  :mode-line-left-separator ""
  :mode-line-right-separator ""
  :mode-line-separator-pad 1
  :interface-font "JetBrainsMono Nerd Font"
  :interface-font-scale 105
  :interface-font-weight bold
  :window-dividers t
  :window-divider-width 1
  :window-divider-face picoline-segment-9
  :run-picom t
  :picom-shadow-radius 18
  :picom-shadow-opacity 1
  :workspace-icon-decorator workspace-decorate-circles
  :fringe-width 1
  :dired-icons t
  :dired-icons-size 1.3
  :bg-alpha 80
  :bg-alpha-enabled t
  :random-wallpapers t
  :wallpaper-collection "nord"
  :face-remaps nil
))

(setq emacs-look-nord-minimal '(
  :theme nord
  :mode-line-theme picoline-theme-nord
  :mode-line-separators t
  :mode-line-icons nil
  :mode-line-outline nil
  :mode-line-height 1.37
  :mode-line-adjust 0.12
  :mode-line-left-separator ""
  :mode-line-right-separator ""
  :mode-line-separator-pad 1
  :interface-font "JetBrainsMono Nerd Font"
  :interface-font-scale 105
  :interface-font-weight bold
  :window-dividers t
  :window-divider-width 1
  :window-divider-face picoline-segment-9
  :run-picom t
  :picom-shadow-radius 0
  :picom-shadow-opacity 0
  :workspace-icon-decorator workspace-decorate-circles
  :fringe-width 1
  :dired-icons t
  :dired-icons-size 1.3
  :bg-alpha 90
  :bg-alpha-enabled nil
  :random-wallpapers t
  :wallpaper-collection "nord"
  :face-remaps nil
))

(setq emacs-look-challenger-deep '(
  :theme challenger-deep
  :mode-line-theme picoline-theme-cascade
  :mode-line-separators t
  :mode-line-icons t
  :mode-line-outline nil
  :mode-line-height 1.0
  :mode-line-adjust 0.0
  :mode-line-left-separator ""
  :mode-line-right-separator ""
  :mode-line-separator-pad 0
  :interface-font "JetBrainsMono Nerd Font"
  :interface-font-scale 105
  :interface-font-weight bold
  :window-dividers t
  :window-divider-width 1
  :window-divider-face picoline-segment-11
  :run-picom t
  :picom-shadow-radius 0
  :picom-shadow-opacity 0
  :workspace-icon-decorator workspace-decorate-circles
  :fringe-width 0
  :dired-icons t
  :dired-icons-size 1.3
  :bg-alpha 95
  :bg-alpha-enabled t
  :random-wallpapers t
  :wallpaper-collection "retrospace"
  :face-remaps nil
))

(setq emacs-look-dracula '(
  :theme dracula
  :mode-line-theme picoline-theme-dracula
  :mode-line-separators t
  :mode-line-icons t
  :mode-line-outline t
  :mode-line-height 1.1
  :mode-line-adjust 0.05
  :mode-line-left-separator ""
  :mode-line-right-separator ""
  :mode-line-separator-pad 0
  :interface-font "Iosevka Term"
  :interface-font-scale 120
  :interface-font-weight medium
  :window-dividers t
  :window-divider-width 1
   :window-divider-face picoline-segment-5
  :run-picom t
  :picom-shadow-radius 0
  :picom-shadow-opacity 0
  :workspace-icon-decorator workspace-decorate-circles
  :fringe-width 1
  :dired-icons t
  :dired-icons-size 1.2
  :bg-alpha 80
  :bg-alpha-enabled t
  :random-wallpapers t
  :wallpaper-collection "retrospace"
  :face-remaps nil
))

(setq emacs-look-monokai '(
  :theme monokai
  :mode-line-theme picoline-theme-monokai-pro
  :mode-line-separators t
  :mode-line-icons nil
  :mode-line-outline nil
  :mode-line-height 1.2
  :mode-line-adjust 0.11
  :mode-line-left-separator ""
  :mode-line-right-separator ""
  :mode-line-separator-pad 0
  :interface-font "Source Code Pro"
  :interface-font-scale 105
  :interface-font-weight semibold
  :window-dividers t
  :window-divider-width 1
  :window-divider-face picoline-segment-8
  :run-picom t
  :picom-shadow-radius 0
  :picom-shadow-opacity 0
  :workspace-icon-decorator workspace-decorate-circles
  :fringe-width 1
  :dired-icons t
  :dired-icons-size 1.3
  :bg-alpha 85
  :bg-alpha-enabled nil
  :random-wallpapers t
  :wallpaper-collection "abstract"
  :face-remaps
  ((mode-line-inactive :background mode-line :background)
   (mode-line :background font-lock-comment-delimiter-face :foreground))
))

(setq emacs-look-black-metal '(
  :theme base16-black-metal-mayhem
  :mode-line-theme picoline-theme-black-metal
  :mode-line-separators nil
  :mode-line-icons nil
  :mode-line-outline nil
  :mode-line-height 1
  :mode-line-adjust 0.0
  :mode-line-left-separator nil
  :mode-line-right-separator nil
  :mode-line-separator-pad 0
  :interface-font "Terminus"
  :interface-font-scale 120
  :interface-font-weight bold
  :window-dividers nil
  :window-divider-width 1
  :window-divider-face picoline-segment-8
  :run-picom t
  :picom-shadow-radius 0
  :picom-shadow-opacity 0
  :workspace-icon-decorator workspace-decorate-numbers
  :fringe-width 0
  :dired-icons nil
  :dired-icons-size 1.3
  :bg-alpha 80
  :bg-alpha-enabled nil
  :random-wallpapers nil
  :wallpaper-collection nil
  :face-remaps nil
))

(setq emacs-look-file-path (concat (getenv "HOME") "/.emacs.d/emacs-look"))
(setq emacs-look-default emacs-look-nord)

(defun emacs-look-set-font (font size weight)
  (set-face-attribute 'default nil
    :font font
    :weight weight
    :width 'normal
    :height size))

(defun emacs-look-write-to-file (look)
  (write-region (format "%S" look) nil emacs-look-file-path))

(defun emacs-look-read-from-file ()
  (if (file-exists-p emacs-look-file-path)
  (emacs-look-set-look (car (read-from-string
    (with-temp-buffer (insert-file-contents emacs-look-file-path)
                      (buffer-string)))))
  (emacs-look-set-look emacs-look-default)))

(defun emacs-look-remap-faces (look)
  (mapc
   (lambda (elem)
          (set-face-attribute (nth 0 elem) nil (nth 1 elem)
                              (face-attribute (nth 2 elem) (nth 3 elem))))
        (plist-get look :face-remaps)))

(defun emacs-look-set-look (look)
  ;Setting up variables
  (setq mode-line-draw-separators (eval (plist-get look :mode-line-separators)))
  (setq mode-line-show-icons (eval (plist-get look :mode-line-icons)))
  (setq mode-line-draw-outline (eval (plist-get look :mode-line-outline)))
  (setq window-divider-draw-edges (eval (plist-get look :mode-line-outline)))
  (setq mode-line-height-multiplier (plist-get look :mode-line-height))
  (setq mode-line-height-adjust (plist-get look :mode-line-adjust))
  (setq mode-line-left-separator (plist-get look :mode-line-left-separator))
  (setq mode-line-right-separator (plist-get look :mode-line-right-separator))
  (setq mode-line-separator-padding (plist-get look :mode-line-separator-pad))
  (setq window-divider-width-chars (plist-get look :window-divider-width))
  (setq window-divider-edge-face (plist-get look :window-divider-face))
  (setq picom-shadow-radius (plist-get look :picom-shadow-radius))
  (setq picom-shadow-opacity (plist-get look :picom-shadow-opacity))
  (setq workspace-decorate-icon-function
   (plist-get look :workspace-icon-decorator))
  (setq fringe-mode-char-width (plist-get look :fringe-width))
  (setq micro-thumbs-show-icons (eval (plist-get look :dired-icons)))
  (setq micro-thumbs-icon-height (plist-get look :dired-icons-size))
  (setq bg-alpha (plist-get look :bg-alpha))
  (setq bg-alpha-default-enabled (eval (plist-get look :bg-alpha-enabled)))
  (setq wallpaper-change-wallpapers (eval (plist-get look :random-wallpapers)))
  ;Running setup
  (load-theme (plist-get look :theme) t)
  (emacs-look-set-font (plist-get look :interface-font)
    (plist-get look :interface-font-scale)
    (plist-get look :interface-font-weight))
  (when (plist-get look :wallpaper-collection)
    (wallpaper-set-current-collection (plist-get look :wallpaper-collection)))
  (when wallpaper-change-wallpapers (wallpaper-set-random-wallpaper))
  (fringe-mode (* (default-font-width) fringe-mode-char-width))
  (picoline-set-theme (eval (plist-get look :mode-line-theme)))
  (workspace-linemode-update-string)
  (whitespace-mode-fix-faces)
  (window-divider-set-faces)
  (display-fill-column-indicator-mode-fix-faces)
  (emacs-look-remap-faces look)
  (if (eval (plist-get look :window-dividers))
    (window-divider-mode t)
    (window-divider-mode 0))
  (mode-line-regenerate)
  (bg-alpha-setup-frame nil)
  (picom-update-settings)
  (when (eval (plist-get look :run-picom)) (start-picom)))

(defun emacs-look-set ()
  (interactive)
  (let ((look (read-from-minibuffer "Set look: ")))
    (if (boundp (car (read-from-string look)))
        (progn
          (emacs-look-set-look (eval (car
            (read-from-string look))))
          (emacs-look-write-to-file (eval (car
            (read-from-string look))))
          (message (concat "Set current look to: " look)))
      (message "No such emacs look!"))))

(emacs-look-read-from-file)
